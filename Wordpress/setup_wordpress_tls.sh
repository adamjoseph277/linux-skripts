#!/bin/bash

if [ "$EUID" -ne 0 ]
  then echo "Please run as root"
  exit
fi


echo -n 'Enter your Domain: ' 
read servername
echo

echo -n 'Enter the Password for the mysql Password (default: m158): ' 
read -s mysql_password
echo

if [ -z "$mysql_password" ]
then
  mysql_password="m158"
fi

apt update -y 
apt install apache2 -y \
                 ghostscript \
                 libapache2-mod-php \
                 mysql-server \
                 php \
                 php-bcmath \
                 php-curl \
                 php-imagick \
                 php-intl \
                 php-json \
                 php-mbstring \
                 php-mysql \
                 php-xml \
                 php-zip \
                 sudo \
                 curl


mkdir -p /srv/www
chown www-data: /srv/www
curl https://wordpress.org/latest.tar.gz | sudo -u www-data tar zx -C /srv/www

cp ./wordpress.conf /etc/apache2/sites-available/wordpress.conf

sed -i "s/<Servername>/$servername/g" "/etc/apache2/sites-available/wordpress.conf"

a2ensite wordpress
a2enmod rewrite
a2dissite 000-default

ip=$(hostname -i)
echo "ServerName $ip" >> '/etc/apache2/apache2.conf'

service apache2 restart
service mysql start

sed -i "s/<your-password>/$mysql_password/g" "./db.sql"
mysql -u root -p < db.sql

#sudo -u www-data cp /srv/www/wordpress/wp-config-sample.php /srv/www/wordpress/wp-config.php

#sudo -u www-data sed -i 's/database_name_here/wordpress/' /srv/www/wordpress/wp-config.php
#sudo -u www-data sed -i 's/username_here/wordpress/' /srv/www/wordpress/wp-config.php
#sudo -u www-data sed -i "s/password_here/$mysql_password/" /srv/www/wordpress/wp-config.php

